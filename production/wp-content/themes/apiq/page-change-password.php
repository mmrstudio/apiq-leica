<?php
    
    /* Template Name: Page - Change Password */

    get_header();

    if ( have_posts() ) while ( have_posts() ) : the_post();
    
?>

    <div class="container no-padding">

        <div class="home-hero">
            <div class="apiq-title"></div>
            <div class="apiq-banner"></div>
            <div class="leica-brand-stripe"></div>
        </div>

    </div>

    <div class="container home-main">

        <div class="row title-row">

            <div class="col-xs-6 title-col home-left-col">
                <h1><?php _e('Change Password', 'apiq'); ?></h1>
            </div>

            <div class="col-xs-6 title-col home-right-col">
            </div>

        </div>

        <div class="row content-row">

            <div class="col-xs-6 intro home-left-col" id="introCol">
                <p><?php _e('Please use the form to change your login password.', 'apiq'); ?></p>
            </div>

            <div class="col-xs-6 home-right-col login-resource-col">

                <form action="/" class="change-password-form" id="changePasswordForm">
                
                    <div class="form-group login-password">
                        <input type="password" class="form-control input-large" id="loginPassword1" placeholder="<?php _e('New Password', 'apiq'); ?>">
                    </div>
                
                    <div class="form-group login-password">
                        <input type="password" class="form-control input-large" id="loginPassword2" placeholder="<?php _e('Confirm Password', 'apiq'); ?>">
                    </div>
                
                    <div class="form-group submit">
                        <?php wp_nonce_field( 'ajax-change-password-nonce', 'wp_nonce' ); ?>
                        <button type="submit" class="login-button btn btn-primary btn-lg btn-thin-arrow-right" id="changePasswordButton"><?php _e('Submit', 'apiq'); ?></button>
                    </div>

                </form>

            </div>

        </div>

    </div>

<?php

    endwhile; // end loop

    get_footer();
