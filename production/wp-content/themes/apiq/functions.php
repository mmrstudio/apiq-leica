<?php

    // Load extra helper functions
    require_once (TEMPLATEPATH . '/includes/helper_functions.php');
    require_once (TEMPLATEPATH . '/includes/ajax_functions.php');

    // Load custom post types
    require_once (TEMPLATEPATH . '/includes/classes/CPT.php');
    require_once (TEMPLATEPATH . '/includes/custom-post-types/resources.cpt.php');

    // translation fields
    require_once (TEMPLATEPATH . '/includes/translation_fields.php');

    // Set default CONSTANTS
    define( 'THEME_URL' , get_template_directory_uri() );
    define( 'PAGE_BASENAME' , get_current_basename() );

    // Add theme supports
    add_theme_support('post-thumbnails');

    // Register navigation
    //register_nav_menu( 'main-nav' , 'Main Menu' );

    // Create user roles
    add_role('apiq_cloud', 'APiQ Cloud User', array('read' => true));
    add_role('associate', 'Associate', array('read' => true));

    // Add image sizes
    add_image_size('resource-thumb', 270 , 200 , TRUE );
    add_image_size('resource-thumb-2x', 540 , 400 , TRUE );

    // Set default image link type
    update_option('image_default_link_type','file');

    // Set JPG quality for image resizing
    add_filter( 'jpeg_quality', 'set_jpg_quality' );
    function set_jpg_quality( $quality ) { return 95; }

    // Load stylesheets and javascript files
    add_action('init', 'theme_init');

    // Initilization function
    function theme_init() {

        // Load stylesheets and javascript files
        load_scripts_and_styles();

        // Create options page
        create_theme_options_page();

        // hide stuff we don't need
        remove_action('wp_head', 'rsd_link');
        remove_action('wp_head', 'wlwmanifest_link');
        remove_action('wp_head', 'wp_generator');
        remove_action('wp_head', 'index_rel_link');
        remove_action('wp_head', 'feed_links');

        // define rewrite rules - english
        add_rewrite_rule('^installation?','index.php?resource_category=installation','top');
        add_rewrite_rule('^training?','index.php?resource_category=training','top');
        add_rewrite_rule('^support?','index.php?resource_category=apiq-support','top');
        add_rewrite_rule('^product-information?','index.php?resource_category=product-information','top');

            // german
            add_rewrite_rule('^ausbildung?','index.php?resource_category=training','top'); // training
            add_rewrite_rule('^unterstützung?','index.php?resource_category=apiq-support','top'); // support
            add_rewrite_rule('^produktinformation?','index.php?resource_category=product-information','top'); // product-information

            // french
            add_rewrite_rule('^formation?','index.php?resource_category=training','top'); // training
            add_rewrite_rule('^soutien?','index.php?resource_category=apiq-support','top'); // support
            add_rewrite_rule('^information-produit?','index.php?resource_category=product-information','top'); // product-information

            // italian
            add_rewrite_rule('^installazione?','index.php?resource_category=installation','top'); // installation
            add_rewrite_rule('^formazione?','index.php?resource_category=training','top'); // training
            add_rewrite_rule('^assistenza?','index.php?resource_category=apiq-support','top'); // support
            add_rewrite_rule('^informazioni-sul-prodotto?','index.php?resource_category=product-information','top'); // product-information

            // spanish
            add_rewrite_rule('^instalación?','index.php?resource_category=installation','top'); // installation
            add_rewrite_rule('^formación?','index.php?resource_category=training','top'); // training
            add_rewrite_rule('^asistencia?','index.php?resource_category=apiq-support','top'); // support
            add_rewrite_rule('^información-del-producto?','index.php?resource_category=product-information','top'); // product-information

        //flush_rewrite_rules();

    }


    function load_scripts_and_styles() {

        if( ! is_admin() ) :

            // Add stylesheets
            wp_enqueue_style( 'main_styles' , THEME_URL . '/style.css' , FALSE , '0.1.0' , 'screen' );

            // Load scripts
            add_action('wp_enqueue_scripts', 'load_scripts', 0);

            // Replace jQuery with Google CDN version
            function load_scripts() {

                // Load jQuery from CDN
                $jquery_version = '1.11.1';
                wp_deregister_script( 'jquery' );
                wp_register_script( 'jquery', '//ajax.googleapis.com/ajax/libs/jquery/' . $jquery_version . '/jquery.min.js' , array() , $jquery_version , FALSE );
                wp_enqueue_script( 'jquery' );

                // Load modernizr
                wp_enqueue_script( 'modernizr' , THEME_URL . '/js/vendor/modernizr.min.js' , '' , '0.1.0' , FALSE );

                // Load main scripts
                wp_enqueue_script( 'plugins_script' , THEME_URL . '/js/plugins.min.js' , 'jquery' , '0.1.0' , TRUE );
                wp_enqueue_script( 'main_script' , THEME_URL . '/js/main.min.js' , 'jquery' , '0.1.0' , TRUE );
            }

        endif;

    }

    function create_theme_options_page() {

        if( function_exists('acf_add_options_page') ) {

        	$page = acf_add_options_page(array(
        		'page_title' 	=> 'Header &amp; Footer',
        		'menu_title' 	=> 'Header &amp; Footer',
        		'menu_slug' 	=> 'header-footer-settings',
        		'capability' 	=> 'edit_posts',
        		'redirect' 	=> false
        	));

        }

    }

    // Increase excerpt length
    add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );
    function custom_excerpt_length( $length ) { return 100; }

    // Filter HTML in Wordpress text widget
    add_filter('widget_text', 'filter_text_widget');
    function filter_text_widget( $text ) { return apply_filters('the_content', $text); }

    // Remove hardcoded width/height attrs from post thumbnails
    add_filter( 'post_thumbnail_html', 'remove_thumbnail_dimensions', 10, 3 );
    function remove_thumbnail_dimensions( $html, $post_id, $post_image_id ) {
        $html = preg_replace( '/(width|height)=\"\d*\"\s/', "", $html );
        return $html;
    }

    // Remove dimension attributes from img tags in content
    add_filter('the_content', 'remove_img_dimensions', 10);
    function remove_img_dimensions($html) {
        $html = preg_replace('/(width|height)=["\']\d*["\']\s?/', "", $html);
        return $html;
    }

    // Add nav menu item names to links
    add_filter( 'nav_menu_css_class', 'add_nav_item_name_class', 10, 2 );
    function add_nav_item_name_class( $classes , $item ) {
        $new_class = strtolower( preg_replace("/[^A-Za-z0-9 ]/", '-', $item->title) );
        $classes[] = $new_class;
        $classes[] = $new_class.'-link';
        return $classes;
    }

    // remove admin menu items
    add_action( 'admin_menu', 'remove_admin_links' );
    function remove_admin_links() {
        remove_menu_page('edit-comments.php');
    }


    // wp-admin login screen logo
    add_action('login_enqueue_scripts', 'admin_login_logo');
    function admin_login_logo() {
        echo '<style> .login #backtoblog a, .login #nav a { color: #000 !important; } .login h1 a { background: url('.THEME_URL.'/images/login-logo.png) no-repeat center center transparent; background-size: auto auto; width: 320px; } </style>'."\n";
    }

    function add_slug_body_class( $classes ) {
        global $post;
        if ( isset( $post ) ) { $classes[] = $post->post_type . '-' . $post->post_name; }
        return $classes;
    }
    add_filter( 'body_class', 'add_slug_body_class' );

    define('FIRST_VISIT', is_first_time());

    add_filter('show_admin_bar', '__return_false');

    add_filter('acf/settings/show_admin', 'my_acf_show_admin');
    function my_acf_show_admin( $show ) {
        return current_user_can('manage_capabilities');
    }

    remove_role('subscriber');
    remove_role('editor');
    remove_role('author');
    remove_role('contributor');

    // get current user info
    $current_user = false;
    $current_username = '';
    $user_type = 'normal';

    if(is_user_logged_in()) :
        $current_user = wp_get_current_user();
        $current_username = $current_user->user_login;

        // format apiq cloud username
        if(strpos($current_username, '__')) :
            $current_username = str_replace('__', '<br>', $current_username);
            $user_type = 'apiq';
            //$current_username_arr = explode('__', $current_username);
            //$current_username = $current_username_arr[1];
        endif;

    endif;

    define('CURRENT_USERNAME', $current_username);
    define('USER_TYPE', $user_type);

/*
    add_action('init','custom_login');
    function custom_login() {
        global $pagenow;
        if( 'wp-login.php' == $pagenow && $_GET['action'] != "logout") :
            wp_redirect(site_url('/'));
            exit();
        endif;
    }
*/

    add_filter( 'retrieve_password_message', 'custom_retrieve_password_message', 10, 2 );
    function custom_retrieve_password_message( $message, $key ) {

        $user_data = '';
        // If no value is posted, return false
        if( ! isset( $_POST['user_login'] )  ){
                return '';
        }
        // Fetch user information from user_login
        if ( strpos( $_POST['user_login'], '@' ) ) {

            $user_data = get_user_by( 'email', trim( $_POST['user_login'] ) );
        } else {
            $login = trim($_POST['user_login']);
            $user_data = get_user_by('login', $login);
        }
        if( ! $user_data  ){
            return '';
        }
        $user_login = $user_data->user_login;
        $user_email = $user_data->user_email;

        // Setting up message for retrieve password
        $message = __('It looks like you want to reset your password!', 'apiq') . "\n\n";
        $message .= __('Please visit this link:', 'apiq') . "\n\n";
        $message .= site_url("/reset-password?action=rp&key=$key&login=" . rawurlencode($user_login), 'login');

        // Return completed message for retrieve password
        return $message;
    }



    add_filter( 'wp_mail_from', 'custom_wp_mail_from' );
    function custom_wp_mail_from( $original_email_from ) {
    	return 'admin@apiq.com';
    }


    add_filter( 'wp_mail_from_name', 'custom_wp_mail_from_name' );
    function custom_wp_mail_from_name( $original_email_from ) {
    	return 'APiQ.com';
    }

    add_action( 'phpmailer_init', 'apiq_phpmailer_init' );
    function apiq_phpmailer_init( PHPMailer $phpmailer ) {
        $phpmailer->Host = 'smtp.mandrillapp.com';
        $phpmailer->Port = 587;
        $phpmailer->Username = 'web@mmr.com.au';
        $phpmailer->Password = 'dYEUdj4uVAKad61TjU8DdQ';
        $phpmailer->SMTPAuth = true;

        $phpmailer->IsSMTP();
    }

    add_filter( 'login_url', 'homepage_login', 10, 2 );
    function homepage_login( $login_url, $redirect ) {
        $redirect = (strlen($redirect) > 0) ? '?redirect_to=' . $redirect : '';
        return home_url($redirect);
    }

    add_filter('init','apiq_set_locale', 10);
    function apiq_set_locale() {

        if(! isset($_GET['locale'])) return;

        $locale = $_GET['locale'];

        if(in_array($locale, array('en_US', 'de_DE', 'fr_FR', 'it_IT', 'es_ES'))) :

            // save locale in session
            setcookie('apiq_locale', $locale, time()+3600*24*100, COOKIEPATH, COOKIE_DOMAIN, false);

        endif;

        // get current url to redirect to
        $url = strtok($_SERVER["REQUEST_URI"],'?');
        wp_redirect($url); exit;

    }

    add_filter('locale','apiq_change_locale', 10);
    function apiq_change_locale($locale) {
        $locale = (isset($_COOKIE['apiq_locale'])) ? $_COOKIE['apiq_locale'] : 'en_US';
        return $locale;
    }

    add_action('after_setup_theme', 'apiq_load_lang', 11);
    function apiq_load_lang() {
        $lang_path = get_template_directory() . '/languages';
        //echo get_locale(); exit;
        if(! load_theme_textdomain('apiq', $lang_path)) :
            echo 'Could not load language: ' . get_locale();
        endif;
    }

    if(isset($_GET['lang']) && ! is_admin()) :
        $_SESSION["active_language"] = $_GET['lang'];
        $uri_parts = explode('?', $_SERVER['REQUEST_URI'], 2);
        $redirect = 'http://' . $_SERVER['HTTP_HOST'] . $uri_parts[0];
        wp_redirect($redirect); exit;
    else :
        if(! isset($_SESSION["active_language"])) :
            $_SESSION["active_language"] = 'en';
        endif;
    endif;

    add_action('wp_loaded', 'apiq_set_current_language');
    function apiq_set_current_language($lang) {
        if(! is_admin()) :
            global $sitepress;
            $lang = isset($_SESSION["active_language"]) ? $_SESSION["active_language"] : 'en';
            $sitepress->switch_lang($lang, true);
        endif;
    }
