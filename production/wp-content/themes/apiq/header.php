<?php //global $wp_query; print_r($wp_query); exit; ?><!DOCTYPE HTML>

<html lang="en">

    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

        <title><?php echo get_page_title(); ?></title>

        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<?php wp_head(); ?>

        <script>
            var root_url = '<?php echo site_url(); ?>';
            var wp_ajax_url = '<?php echo site_url('/wp-admin/admin-ajax.php'); ?>';
        </script>

        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <?php //echo google_analytics('', 'auto'); ?>

    </head>

    <body <?php body_class(); ?>>

        <div class="nav-dismiss" id="responsiveNavDismiss"></div>

        <nav class="navbar">

            <button class="nav-toggle icon icon-menu" id="responsiveNavToggle"></button>

            <div class="container-fluid navbar-wrap">

                <div class="container navbar-inner">

                    <div class="navbar-brand">

                        <div class="logo-wrap">
                            <div class="leica-biosystems-logo"><?php _e('Leica Biosystems', 'apiq'); ?></div>
                        </div>

                    </div>

                    <div class="navbar-app-user <?php if( ! is_user_logged_in() ) : ?>hidden<?php endif; ?>" id="currentUser">
                        <div class="navbar-app-user-inner">

                            <div class="user-control">
                                <a href="#" class="dropdown-toggle username" id="currentUserName"><?php echo CURRENT_USERNAME; ?></a>
                                <?php if( is_user_logged_in() ) : ?>
                                <ul class="dropdown-menu">
                                    <li><a href="<?php echo wp_logout_url( home_url() ); ?>" id="logoutLink"><?php _e('Logout', 'apiq'); ?></a></li>
                                    <?php if(USER_TYPE == 'normal') : ?>
                                    <li class="divider"></li>
                                    <li><a href="/change-password"><?php _e('Change Password?', 'apiq'); ?></a></li>
                                    <?php endif; ?>
                                </ul>
                                <?php endif; ?>
                            </div>

                        </div>
                    </div>

                    <ul class="nav navbar-nav" id="navbar">
                        <li><a href="<?php echo site_url(''); ?>" class="navbar-home"><span class="label"><?php _e('Home', 'apiq'); ?></span></a></li>
                        <li class="<?php if( ! is_user_logged_in() ) : ?>disabled<?php endif; ?>"><a href="<?php echo site_url((ICL_LANGUAGE_CODE != 'en' ? '/' . ICL_LANGUAGE_CODE : '') . '/' . __('installation', 'apiq')); ?>" class="navbar-installation"><span class="label"><?php _e('Installation', 'apiq'); ?></span></a></li>
                        <li class="<?php if( ! is_user_logged_in() ) : ?>disabled<?php endif; ?>"><a href="<?php echo site_url((ICL_LANGUAGE_CODE != 'en' ? '/' . ICL_LANGUAGE_CODE : '') . '/' . __('training', 'apiq')); ?>" class="navbar-training"><span class="label"><?php _e('Training', 'apiq'); ?></span></a></li>
                        <li class="<?php if( ! is_user_logged_in() ) : ?>disabled<?php endif; ?>"><a href="<?php echo site_url((ICL_LANGUAGE_CODE != 'en' ? '/' . ICL_LANGUAGE_CODE : '') . '/' . __('support', 'apiq')); ?>" class="navbar-support"><span class="label"><?php _e('Support', 'apiq'); ?></span></a></li>
                        <li class="<?php if( ! is_user_logged_in() ) : ?>disabled<?php endif; ?>"><a href="<?php echo site_url((ICL_LANGUAGE_CODE != 'en' ? '/' . ICL_LANGUAGE_CODE : '') . '/' . __('product-information', 'apiq')); ?>" class="navbar-product-information"><span class="label"><?php _e('Product Information', 'apiq'); ?></span></a></li>
                    </ul>

                </div>

            </div>

        </nav>
