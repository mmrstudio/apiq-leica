<?php

	### HELPER FUNCTIONS ###

	// Simple function to return false in wordpress callbacks
	function return_false() { return FALSE; }

	// Function to build HTML5 data attributes (eg. <tag data-key="value">)
	function build_html_data_attributes( $data_arr=array() ) {
		$data_processed_array = array();
		foreach( $data_arr as $key=>$val ) : $data_processed_array[] = 'data-'.$key.'="'.$val.'"'; endforeach;
		return implode(' ', $data_processed_array);
	}

	// Function get current URL basename
	function get_current_basename() {
		$url = explode('/', 'http://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
		$base = $url[3];
		return $base;
	}

	// Function to get image attachment ID from its link
	function get_image_id_by_link($link) {
		global $wpdb;
		$link = preg_replace('/-\d+x\d+(?=\.(jpg|jpeg|png|gif)$)/i', '', $link);
		return $wpdb->get_var("SELECT ID FROM {$wpdb->posts} WHERE guid='$link'");
	}

	// Content navigation
	function _posts_archive_nav( $prev_str='&larr; Older posts', $next_str='Newer posts &rarr;' )
		{
			global $wp_query;

			if ( $wp_query->max_num_pages > 1 ) : ?>
				<nav class="content_nav">
					<div class="nav-previous"><?php next_posts_link($prev_str); ?></div>
					<div class="nav-next"><?php previous_posts_link($next_str); ?></div>
				</nav>
			<?php endif;
		}

	// Next/Prev post navigation
	function _post_nav( $prev_str='%link', $next_str='%link' )
		{
			global $wp_query;

		?>
				<nav class="post_nav">
					<div class="nav-previous"><?php previous_post_link($prev_str, '&larr; Previous'); ?></div>
					<div class="nav-next"><?php next_post_link($next_str, 'Next &rarr;'); ?></div>
				</nav>
			<?php
		}

	// Read more link
	add_filter('excerpt_more', 'new_excerpt_more');
	function new_excerpt_more($more) { return '... <br />'; }

	// Custom Fields

	function get_custom_fields($post_id) {
		$custom_fields = array();

		$get_custom_fields = get_post_custom($post_id);

		if($get_custom_fields) :

			foreach($get_custom_fields as $field_key => $field_val) :
				$custom_fields[$field_key] = $field_val[0];
			endforeach;

		endif;

		return $custom_fields;
	}


			function custom_field($key, $fields) {
				$custom_field = '';

				if(array_key_exists($key,$fields)) :
					$custom_field = $fields[$key];
				endif;

				return $custom_field;
			}


	// Check if iOS device
	function is_iOS() {
		if (strpos($_SERVER['HTTP_USER_AGENT'], 'iPad') ||
			strpos($_SERVER['HTTP_USER_AGENT'], 'iPhone') ||
			strpos($_SERVER['HTTP_USER_AGENT'], 'iPod')) :
			return TRUE;
		else :
			return FALSE;
		endif;
	}

	// Check for H1 tag
	function content_has_h1($content)
		{
			$has_h1 = FALSE;
			if (preg_match_all("=<h1[^>]*>(.*)</h1>=siU", $content, $matches)) {
				$has_h1 = TRUE;
			}
			return $has_h1;
		}

	// Simple BB code parser

	function bb($text) {
		$array = array(
			'[C]' => '&copy;',
			'[b]' => '<strong>',
			'[/b]' => '</strong>',
			'[i]' => '<em>',
			'[/i]' => '</em>',
			'[u]' => '<span class="underline">',
			'[/u]' => '</span>',
			'[center]' => '<span class="center">',
			"[/center]" => "</span>",
			'[left]' => '<span class="left">',
			"[/left]" => "</span>",
			'[right]' => '<span class="right">',
			"[/right]" => "</span>"
		);

		$newtext = str_replace( array_keys($array) , array_values($array) , $text );
		return $newtext;
	}


	// New line to paragraphs

	function nl2p($html,$indentation='') {
		$html = preg_replace("/(\r\n|\n|\r)/", "\n", $html);
		$html = preg_replace("/\n\n+/", "\n\n", $html);
		$html = preg_replace('/\n?(.+?)(\n\n|\z)/s', $indentation."<p>$1</p>", $html);
		$html = preg_replace('|(?<!</p> )\s*\n|', "<br />", $html);
		$html = str_replace('</p>','</p>'."\n",$html);
		return $html;
	}


	// Truncate text to number of words specified

	function limit_words($str,$num_words=50,$suffix='...') {
		$str_array = explode(' ',$str);
		$return_str = '';

		for($w=0; $w<$num_words; $w++) {
			$return_str .= $str_array[$w].' ' ;
		}

		if(count($str_array) > $num_words) {
			$return_str .= $suffix;
		}

		return $return_str ;
	}


	function first_paragraph($text, $isHTML = true) {

		$result = $text;

		if( $isHTML ) :

			// convert line breaks/paragraphs
			$result = str_replace("\n", "", $result); // remove excess \n
			$result = str_replace("<br>", "\n", $result);
			$result = str_replace("<br/>", "\n", $result);
			$result = str_replace("<br />", "\n", $result);
			$result = str_replace("</p>", "\n\n", $result);

			// strip all remaining tags
			$result = strip_tags($result);

		endif;

		// try and return the first paragraph, if I can't, return all of it
		$paragraphs = explode("\n\n", trim($result));

		if(count($paragraphs) > 1) :
			return nl2br(trim($paragraphs[0]));
		else :
			return $text;
		endif;

	}

	// Function to load ACF gallery in post
    function load_gallery( $gallery_field , $post_id ) {

        $gallery_images = array();

        if( get_field( $gallery_field , $post_id ) ) :

            $gallery = get_field( $gallery_field , $post_id );

            //print_r($gallery); exit;

            foreach( $gallery as $image ) :

                $gallery_image = array(
                    'full' => $image['url'],
                    'w' => $image['width'],
                    'h' => $image['height'],
                    'thumbnail' => $image['sizes']['gallery-thumb'],
                    'thumbnail-2x' => $image['sizes']['gallery-thumb-2x'],
                    'thumb_w' => $image['sizes']['gallery-thumb-width'],
                    'thumb_h' => $image['sizes']['gallery-thumb-height'],
                );

                $gallery_images[] = $gallery_image;

            endforeach;

        endif;

        //print_r($gallery_images); exit;

        return $gallery_images;

    }

	function build_acf_gallery_fancybox( $images , $gallery_name , $container='div' ) {

		if( is_array($images) AND count($images) > 0 ) :

			echo "\n".'<' . $container . ' class="gallery ' . $gallery_name . '-gallery">'."\n";

			foreach( $images as $img ) :

				echo '	<a href="' . $img['url'] . '" class="fancybox-thumb" rel="' . $gallery_name . '-gallery" title="' . $img['title'] . '"><img src="' . $img['sizes']['thumbnail'] . '" alt="' . $img['title'] . '"></a>'."\n";


			endforeach;

			echo '</' . $container . '>'."\n\n";

		endif;

	}


	// Get file size function
	function get_file_size($file) {
		$file_size = '';
		if(is_file($file)) :
			$file_size = filesize($file);
			if($file_size < 1024) :
				$file_size = $file_size.'B';
			elseif($file_size >= 1024 AND $file_size < 1048576) :
				$file_size = intval(($file_size / 1024)). 'KB';
			elseif($file_size >= 1048576) :
				$file_size = intval(($file_size / 1048576)).'MB';
			endif;
		endif;

		return $file_size;
	}

	// Prints copyright range (eg. 2007-2011)
	function copyright_range($first_year, $separator) {
		$range = '';
		$current_year = date("Y");
		if($first_year < $current_year) :
			$range = $first_year.$separator.$current_year;
		else :
			$range = $current_year;
		endif;
		return $range;
	}

	// Add additional class to body class from outside header.php
	function add_body_class($additional_class) {
		global $add_additional_class;
		$add_additional_class = $additional_class;
		// Add specific CSS class by filter
		add_filter('body_class', 'add_additional_class');

		if( ! function_exists('add_additional_class')) :
			function add_additional_class($classes) {
				global $add_additional_class;
				// add 'class-name' to the $classes array
				$classes[] = $add_additional_class;
				// return the $classes array
				return $classes;
			}
		endif;
	}

	// Replace tags in footer text
	function write_footer_text($text) {
		$text = str_replace('[C]', '&copy', $text);
		$text = str_replace('[year]', date('Y'), $text);
		return $text;
	}


	function sibling_pages_menu( $post_parent ) {

		wp_list_pages(array(
			'depth'        => '2',
			'child_of'     => $post_parent,
			'title_li'     => '',
			'echo'         => 1,
			'sort_column'  => 'menu_order, post_title',
			'post_type'    => 'page',
			'post_status'  => 'publish'
		));

	}

	// List of Australian States
	function states_list() {
		return array(
			'ACT' => 'Australian Capital Territory',
			'NSW' => 'New South Wales',
			'NT'  => 'Northern Territory',
			'QLD' => 'Queensland',
			'SA'  => 'South Australia',
			'TAS' => 'Tasmania',
			'VIC' => 'Victoria',
			'WA'  => 'Western Australia'
		);
	}

	function google_analytics( $key='', $url='' ) {

		return "
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', '".$key."', '".$url."');
  ga('send', 'pageview');

</script>
		";

	}

	function theme_file( $file ) {
		return THEME_URL . '/' . $file;
	}

	function twitterify($ret) {
		$ret = preg_replace("#(^|[\n ])([\w]+?://[\w]+[^ \"\n\r\t< ]*)#", "\\1<a href=\"\\2\" target=\"_blank\">\\2</a>", $ret);
		$ret = preg_replace("#(^|[\n ])((www|ftp)\.[^ \"\t\n\r< ]*)#", "\\1<a href=\"http://\\2\" target=\"_blank\">\\2</a>", $ret);
		$ret = preg_replace("/@(\w+)/", "<a href=\"http://www.twitter.com/\\1\" target=\"_blank\">@\\1</a>", $ret);
		$ret = preg_replace("/#(\w+)/", "<a href=\"https://twitter.com/search?q=\\1\" target=\"_blank\">#\\1</a>", $ret);
		return $ret;
	}


	function first_post_image() {
		global $post, $posts;
		$first_img = '';
		ob_start();
		ob_end_clean();
		$output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
		$first_img = $matches[1][0];
		if(empty($first_img)) {
			$first_img = FALSE;
		}
		return $first_img;
	}

    function removeEmoji($text) {

        $clean_text = "";

        // Match Emoticons
        $regexEmoticons = '/[\x{1F600}-\x{1F64F}]/u';
        $clean_text = preg_replace($regexEmoticons, '', $text);

        // Match Miscellaneous Symbols and Pictographs
        $regexSymbols = '/[\x{1F300}-\x{1F5FF}]/u';
        $clean_text = preg_replace($regexSymbols, '', $clean_text);

        // Match Transport And Map Symbols
        $regexTransport = '/[\x{1F680}-\x{1F6FF}]/u';
        $clean_text = preg_replace($regexTransport, '', $clean_text);

        return $clean_text;
    }


    function output_main_nav($depth=1) {

        wp_nav_menu( array(
            'menu_class' => '',
            'container' => 'none',
            'theme_location' => 'main-nav',
            'depth' => $depth
        ));

    }


    function isAjax() {
        return (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');
    }

    function is_first_time() {
        if(session_id() == '') session_start();
        if (isset($_SESSION['_wp_first_time'])) return false;
        $_SESSION['_wp_first_time'] = true;
        return true;
    }

    function get_page_title() {

        if( is_front_page() ) :
            return __(get_bloginfo('name'), 'apiq');
        elseif(is_tax('resource_category')) :
            $term = get_term_by('slug', get_query_var('term'), get_query_var('taxonomy'));
            return __($term->name, 'apiq') . ' | ' . __(get_bloginfo('name'), 'apiq');
        else :
            return wp_title( '|', false, 'right' ) . __(get_bloginfo('name'), 'apiq');
        endif;

    }
