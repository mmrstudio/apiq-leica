<?php

    function add_ajax_action($action, $callback) {
        add_action('wp_ajax_'.$action, $callback);
        add_action('wp_ajax_nopriv_'.$action, $callback);
    }

    function request_param($key, $default) {
        return (isset($_REQUEST[$key])) ? $_REQUEST[$key] : $default ;
    }

    function ajax_check_user_logged_in() {
        echo (is_user_logged_in()) ? json_encode(true) : json_encode(false);
        exit;
    }

    add_ajax_action('check_user_logged_in', 'ajax_check_user_logged_in');

    function ajax_current_logged_in_user() {

        if(is_user_logged_in()) :

            $get_current_user = wp_get_current_user();

            $user = array(
                'id' => $get_current_user->data->ID,
                'name' => $get_current_user->data->display_name,
                'username' => $get_current_user->data->user_login,
                'email' => $get_current_user->data->user_email,
                'registered' => $get_current_user->data->user_registered,
                'roles' => $get_current_user->roles,
                'admin' => $get_current_user->caps['administrator']
            );

            echo json_encode($user);

        else :
            header('HTTP/1.1 403 Forbidden');
        endif;

        exit;
    }

    add_ajax_action('get_current_user', 'ajax_current_logged_in_user');

    function ajax_signon() {

        $login = array(
            'result' => false,
            'user' => '',
            'message' => '',
            'logout_url' => '/'
        );

        if( ! is_user_logged_in() ) :
            //echo 'User not logged in!'; exit;

            $credentials = array();
            $credentials['user_login']    = !empty( $_POST['username'] ) ? $_POST['username'] : null;
            $credentials['user_password'] = !empty( $_POST['password'] ) ? $_POST['password'] : null;
            $credentials['remember']      = !empty( $_POST['rememberme'] ) ? $_POST['rememberme'] : null;
            $credentials['wp_nonce']      = !empty( $_POST['wp_nonce'] ) ? $_POST['wp_nonce'] : null;

            //print_r($credentials); exit;

            if(wp_verify_nonce( $_POST['wp_nonce'], 'ajax-login-nonce' )) :

                if(strlen(trim($credentials['user_login'])) == 0 || strlen(trim($credentials['user_password'])) == 0) :

                    $login['message'] = 'Incorrect username or password.';

                else :

                    $user = wp_signon($credentials, false);

                    if( ! is_wp_error( $user ) ) :

                        $login['result'] = true;
                        $login['user'] = $user->data->user_login;
                        $login['logout_url'] = wp_logout_url(home_url());

                    else :

                        if (in_array($user->get_error_code(), array('invalid_username', 'incorrect_password', 'empty_username', 'empty_password'))) :
                            $login['message'] = 'Incorrect username or password.';
                        else :
                            $login['message'] = 'An error occurred attempting to authenticate. Please try again later.';
                        endif;

                    endif;

                endif;

            else :
                $login['message'] = 'An error occurred attempting to authenticate. Please try again later.';
            endif;

        else :
            $login['message'] = 'Already logged in.';
        endif;

        echo json_encode($login); exit;

    }

    add_ajax_action('login', 'ajax_signon');

    function ajax_cloud_login() {

        $login = array(
            'result' => false,
            'user' => '',
            'message' => '',
            'logout_url' => '/'
        );

        if( ! is_user_logged_in() ) :
            //echo 'User not logged in!'; exit;

            $credentials = array();
            $credentials['site_name']     = !empty( $_POST['sitename'] ) ? $_POST['sitename'] : null;
            $credentials['user_login']    = !empty( $_POST['username'] ) ? $_POST['username'] : null;
            $credentials['user_password'] = !empty( $_POST['password'] ) ? $_POST['password'] : null;

            // create unique user info
            $credentials['username'] = $credentials['site_name'] . '__' . $credentials['user_login'];
            $credentials['email'] = $credentials['username'] . '@apiq.com';

            //print_r($credentials); exit;

            if(wp_verify_nonce( $_POST['wp_nonce'], 'ajax-login-nonce' )) :

                $existing_user = username_exists($credentials['username']);

                if($existing_user) :

                    // sign in user
                    wp_clear_auth_cookie();
                    wp_set_current_user($existing_user);
                    wp_set_auth_cookie($existing_user, true);

                    $login['result'] = true;
                    $login['user'] = $user->data->user_login;
                    $login['logout_url'] = wp_logout_url(home_url());

                else :

                    // create user
                    $user_data = array(
                        'user_login' => $credentials['username'],
                        'user_email' => $credentials['email'],
                        'user_pass' => isset($credentials['user_password']) ? $credentials['user_password'] : null,
                        'display_name' => $credentials['user_login'],
                    );

                    $user_id = wp_insert_user($user_data);

                    if ( ! is_wp_error($user_id)) :

                        // set role
                        $user = new WP_User($user_id);
                        $user->set_role('apiq_cloud');

                        // save APiQ details
                        update_field('field_56ca9cb5cdc94', true, 'user_' . $user_id); // apiq_user
                        update_field('field_5666098f75993', $credentials['site_name'], 'user_' . $user_id); // site_name

                        // sign in user
                        wp_clear_auth_cookie();
                        wp_set_current_user($user_id);
                        wp_set_auth_cookie($user_id, true);

                        $login['result'] = true;
                        $login['user'] = $credentials['user_login'];
                        $login['logout_url'] = wp_logout_url(home_url());

                    else :

                        $login['message'] = 'An error occurred attempting to authenticate. Please try again later.';

                    endif;

                endif;

            else :
                $login['message'] = 'An error occurred attempting to authenticate. Please try again later.';
            endif;

        else :
            $login['message'] = 'Already logged in.';
        endif;

        echo json_encode($login); exit;

    }

    add_ajax_action('cloud_login', 'ajax_cloud_login');

    function ajax_get_post() {
        $post_id = (isset($_REQUEST['post_id'])) ? $_REQUEST['post_id'] : false ;
        $filter_content = (isset($_REQUEST['filter_content'])) ? $_REQUEST['filter_content'] : false ;

        if($post_id) :
            $post = get_post($post_id, ARRAY_A);
            if($post !== null) :
                if($filter_content) $post['post_content'] = apply_filters('the_content', $post['post_content']);
                $post['permalink'] = get_permalink($post_id);
                echo json_encode($post);
            else :
                header('HTTP/1.1 404 Not Found');
            endif;
        else :
            header('HTTP/1.1 400 Bad Request');
        endif;
        exit;
    }

    add_ajax_action('get_post', 'ajax_get_post');

    function ajax_current_user_can() {

        $capability = (isset($_REQUEST['cap'])) ? $_REQUEST['cap'] : false ;

        // get extra args
        $args = array();
        foreach( $_REQUEST as $key => $value ) :
            if($key != 'action' AND $key != 'cap' AND strpos('_' . $key, 'arg_', 1) ) :
                $args[ str_replace('arg_', '', $key) ] = $value;
            endif;
        endforeach;

        if($capability) :
            echo json_encode( current_user_can($capability, $args) );
        else :
            header('HTTP/1.1 400 Bad Request');
        endif;

        exit;
    }

    add_ajax_action('current_user_can', 'ajax_current_user_can');

    function ajax_check_username_exists() {

        $username = request_param('username', false);

        if( !$username ) :
            header('HTTP/1.1 400 Bad Request'); exit;
        else :
            $username_exists = (username_exists($username) !== null) ? true : false;
        endif;

        echo json_encode($username_exists); exit;
    }

    add_ajax_action('check_username_exists', 'ajax_check_username_exists');


    function ajax_change_password() {

        $change_password = array(
            'result' => false,
            'message' => ''
        );

        $password = $_POST['password'];

        if( is_user_logged_in() ) :

            //print_r($credentials); exit;

            if(wp_verify_nonce( $_POST['wp_nonce'], 'ajax-change-password-nonce' )) :

                if(strlen(trim($password)) == 0) :

                    $change_password['message'] = 'Invalid password entered';

                else :

                    $current_user = wp_get_current_user();

                    wp_set_password($password, $current_user->ID);

                    $change_password['result'] = true;
                    $change_password['message'] = 'Your password has been changed successfully.  You will need to login again.';

                endif;

            else :
                $change_password['message'] = 'An error occurred, please try again later.';
            endif;

        else :
            $change_password['message'] = 'You must be logged in to perform this action';
        endif;

        echo json_encode($change_password); exit;

    }

    add_ajax_action('changePassword', 'ajax_change_password');


    function ajax_retrieve_password() {
    	global $wpdb, $wp_hasher;

        $password_reset = array(
            'result' => false,
            'message' => ''
        );

    	$errors = new WP_Error();

    	if ( empty( $_POST['user_login'] ) ) {
    		$password_reset['message'] = 'Enter a username or e-mail address';
    	} else if ( strpos( $_POST['user_login'], '@' ) ) {
    		$user_data = get_user_by( 'email', trim( $_POST['user_login'] ) );
    		if ( empty( $user_data ) )
                $password_reset['message'] = 'There is no user registered with that email address';
    	} else {
    		$login = trim($_POST['user_login']);
    		$user_data = get_user_by('login', $login);
    	}

    	if ( $errors->get_error_code() ) {
        	$password_reset['message'] = $errors->get_error_code();
    		echo json_encode($password_reset); exit;
        }

    	if ( !$user_data ) {
    		$password_reset['message'] = 'Invalid username or e-mail';
    		echo json_encode($password_reset); exit;
    	}

    	// Redefining user_login ensures we return the right case in the email.
    	$user_login = $user_data->user_login;
    	$user_email = $user_data->user_email;

    	// Generate something random for a password reset key.
    	$key = wp_generate_password( 20, false );

    	// Now insert the key, hashed, into the DB.
    	if ( empty( $wp_hasher ) ) {
    		require_once ABSPATH . WPINC . '/class-phpass.php';
    		$wp_hasher = new PasswordHash( 8, true );
    	}
    	$hashed = $wp_hasher->HashPassword( $key );
    	$wpdb->update( $wpdb->users, array( 'user_activation_key' => $hashed ), array( 'user_login' => $user_login ) );

        $message = "You have requested to reset your APiQ password\n\n";
        $message .= "Please visit the link below:\n\n";
        $message .= site_url("/reset-password?action=rp&key=$key&login=" . rawurlencode($user_login), 'login');

    	$title = 'APiQ.com Password Reset';

    	if(wp_mail( $user_email, wp_specialchars_decode( $title ), $message, 'From: Leica Biosystems <no-reply@apiq.com>' . "\r\n" )) {

        	$password_reset['result'] = true;
        	$password_reset['message'] = 'A password reset email has been sent to the email address assigned to your APiQ.com account.';

    	} else {

            $password_reset['message'] = 'A password reset email has been sent to the email address assigned to your APiQ.com account.';

    	}

        echo json_encode($password_reset); exit;

    }
    add_ajax_action('requestPasswordReset', 'ajax_retrieve_password');


    function ajax_reset_password() {

        //print_r($_POST); exit;

        $change_password = array(
            'result' => false,
            'message' => ''
        );

        $password = $_POST['password'];

        if(wp_verify_nonce( $_POST['wp_nonce'], 'ajax-change-password-nonce' )) :

            if(strlen(trim($password)) == 0) :

                $change_password['message'] = 'Invalid password entered';

            else :

        		$user = check_password_reset_key(trim($_POST['key']), trim($_POST['user_login']));

                //print_r($user); exit;

        		if ( is_wp_error($user) ) :

                    $change_password['message'] = 'Invalid username or key specified';

                else :

                    wp_set_password($password, $user->ID);

                    $change_password['result'] = true;
                    $change_password['message'] = 'Your password has been reset successfully.  <a href="/">Click here</a> to login.';

                endif;

            endif;

        else :
            $change_password['message'] = 'An error occurred, please try again later.';
        endif;

        //print_r($change_password); exit;

        echo json_encode($change_password); exit;

    }

    add_ajax_action('resetPassword', 'ajax_reset_password');
