
        <footer class="footer container">

            <div class="col-xs-12">
                <p>
                    <?php echo bb(__('12/2014 ∙ Copyright [C] by Leica Biosystems Melbourne Pty. Ltd, 2014.', 'apiq')); ?><br>
                    <?php _e('Subject to modifications. LEICA and the Leica Logo are registered trademarks of Leica Microsystems IR GmbH.', 'apiq'); ?><br>
                </p>
                <p class="legal">
                    <a href="/legal-notice/"><?php _e('Legal Notice', 'apiq'); ?></a>
                </p>
                <p class="locale-selector">
                    <strong><?php _e('Language:', 'apiq'); ?></strong>
                    <a href="?lang=en"<?php echo ($_SESSION["active_language"] == 'en') ? ' class="current"' : ''; ?>>English</a>
                    <a href="?lang=de"<?php echo ($_SESSION["active_language"] == 'de') ? ' class="current"' : ''; ?>>Deutsch</a>
                    <a href="?lang=fr"<?php echo ($_SESSION["active_language"] == 'fr') ? ' class="current"' : ''; ?>>Français</a>
                    <a href="?lang=it"<?php echo ($_SESSION["active_language"] == 'it') ? ' class="current"' : ''; ?>>Italiano</a>
                    <a href="?lang=es"<?php echo ($_SESSION["active_language"] == 'es') ? ' class="current"' : ''; ?>>Español</a>
                </p>
            </div>

        </footer>

<?php wp_footer(); ?>

    </body>

</html>
