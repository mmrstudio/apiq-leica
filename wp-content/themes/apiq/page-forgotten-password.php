<?php

    /* Template Name: Page - Forgotten Password */

    get_header();

    if ( have_posts() ) while ( have_posts() ) : the_post();

?>

    <div class="container no-padding">

        <div class="home-hero">
            <div class="apiq-title"></div>
            <div class="apiq-banner"></div>
            <div class="leica-brand-stripe"></div>
        </div>

    </div>

    <div class="container home-main">

        <div class="row title-row">

            <div class="col-xs-6 title-col home-left-col">
                <h1><?php _e('Forgotten Password', 'apiq'); ?></h1>
            </div>

            <div class="col-xs-6 title-col home-right-col">
            </div>

        </div>

        <div class="row content-row">

            <div class="col-xs-6 intro home-left-col" id="introCol">
                <?php if($_GET['type'] === 'apiq') : ?>
                <p><?php _e('Please enter your APiQ.com site name and email address to request a password reset:', 'apiq'); ?></p>
                <?php else : ?>
                <p><?php _e('Please enter the username or email address you use to log into APiQ.com:', 'apiq'); ?></p>
                <?php endif; ?>
            </div>

            <div class="col-xs-6 home-right-col login-resource-col">

                <form action="/" class="change-password-form" id="forgottenPasswordForm" data-login-type="<?php echo $_GET['type']; ?>">

                    <?php if($_GET['type'] === 'apiq') : ?>
                    <div class="form-group login-site-name">
                        <input type="text" class="form-control input-large" id="loginSitename" placeholder="Site Name">
                    </div>
                    <?php endif; ?>

                    <div class="form-group login-username">
                        <input type="text" class="form-control input-large" id="forgottenPasswordUser" placeholder="<?php _e('Username or email address', 'apiq'); ?>">
                    </div>

                    <div class="form-group submit">
                        <?php wp_nonce_field( 'ajax-change-password-nonce', 'wp_nonce' ); ?>
                        <button type="submit" class="login-button btn btn-primary btn-lg btn-thin-arrow-right" id="forgottenPasswordButton"><?php _e('Submit', 'apiq'); ?></button>
                    </div>

                </form>

            </div>

        </div>

    </div>

<?php

    endwhile; // end loop

    get_footer();
