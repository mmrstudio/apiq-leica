<?php
    
//if( function_exists('register_field_group') ):

$languages = array(
    'de_DE' => 'German',
    'fr_FR' => 'French',
    'it_IT' => 'Italian',
    'es_ES' => 'Spanish',
);


$field_group = array (
	'key' => 'group_552b65dbbd521',
	'title' => 'Translations',
	'fields' => array (),
	'location' => array (
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'resource',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
);

foreach($languages as $lang) :

		array (
			'key' => 'field_552b65ea9f1ee',
			'label' => 'German (de_DE)',
			'name' => '',
			'prefix' => '',
			'type' => 'tab',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
		),
		array (
			'key' => 'field_552b662c9f1f0',
			'label' => 'Title',
			'name' => 'title_de_DE',
			'prefix' => '',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_552b65ff9f1ef',
			'label' => 'Description',
			'name' => 'description_de_DE',
			'prefix' => '',
			'type' => 'wysiwyg',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'tabs' => 'all',
			'toolbar' => 'full',
			'media_upload' => 0,
		),
		array (
			'key' => 'field_552b66c9a27d8',
			'label' => 'Alternative Content?',
			'name' => 'alternative_content_de_DE',
			'prefix' => '',
			'type' => 'true_false',
			'instructions' => 'Do you need to provide alternative versions of the resource for this language? This will override the options set above when this language is selected by the user.',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'message' => '',
			'default_value' => 0,
		),
		array (
			'key' => 'field_552b6714a27db',
			'label' => 'Resource Type',
			'name' => 'resource_type_de_DE',
			'prefix' => '',
			'type' => 'radio',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_552b66c9a27d8',
						'operator' => '==',
						'value' => '1',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				'Video' => 'Video',
				'PDF Download' => 'PDF Download',
			),
			'other_choice' => 0,
			'save_other_choice' => 0,
			'default_value' => '',
			'layout' => 'vertical',
		),
		array (
			'key' => 'field_552b66dca27d9',
			'label' => 'PDF Download',
			'name' => 'pdf_download',
			'prefix' => '',
			'type' => 'file',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_552b66c9a27d8',
						'operator' => '==',
						'value' => '1',
					),
					array (
						'field' => 'field_552b6714a27db',
						'operator' => '==',
						'value' => 'PDF Download',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'return_format' => 'array',
			'library' => 'all',
		),
		array (
			'key' => 'field_552b67d53a8b9',
			'label' => 'Video',
			'name' => 'video_de_DE',
			'prefix' => '',
			'type' => 'repeater',
			'instructions' => 'Videos require at least two formats to be uploaded; mp4 and ogg/ogv, with an optional third format; webm.',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_552b66c9a27d8',
						'operator' => '==',
						'value' => '1',
					),
					array (
						'field' => 'field_552b6714a27db',
						'operator' => '==',
						'value' => 'Video',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'min' => '',
			'max' => '',
			'layout' => 'table',
			'button_label' => 'Add Video File',
			'sub_fields' => array (
				array (
					'key' => 'field_552b67ef3a8ba',
					'label' => 'Video File',
					'name' => 'video_file_de_DE',
					'prefix' => '',
					'type' => 'file',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'return_format' => 'array',
					'library' => 'all',
				),
			),
		),
	),

endforeach;

//register_field_group();

//endif;