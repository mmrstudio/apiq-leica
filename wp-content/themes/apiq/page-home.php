<?php

    /* Template Name: Page - Home */

    get_header();

    if ( have_posts() ) while ( have_posts() ) : the_post();

?>

    <div class="container no-padding">

        <div class="home-hero">
            <div class="apiq-title"></div>
            <div class="apiq-banner"></div>
            <div class="leica-brand-stripe"></div>
        </div>

    </div>

    <div class="container home-main">

        <div class="row title-row">

            <div class="col-xs-12 col-md-6 title-col home-left-col">
                <h1><?php echo str_replace('APiQ', '<span>AP<em>iQ</em></span>', __('Welcome to APiQ Customer Support', 'apiq')); ?></h1>
            </div>

            <div class="col-xs-6 title-col home-right-col hidden-xs hidden-sm">
                <h2 id="homeResourcesTitle"><?php _e('Login Details', 'apiq'); ?></h2>
            </div>

        </div>

        <div class="row content-row">

            <div class="col-xs-12 col-md-6 intro home-left-col" id="introCol">
                <?php the_content(); ?>
            </div>

            <div class="col-xs-12 col-md-6 home-right-col login-resource-col" id="loginResourceCol">

                <form action="/" class="login-form" id="loginForm"<?php if(isset($_GET['redirect_to'])) : ?> data-redirect="<?php echo $_GET['redirect_to']; ?>"<?php endif; ?>>

                    <div class="progress" data-size="small" id="loginProgress"></div>

                    <div class="login-form-controls">

                        <div class="form-group login-site-name">
                            <input type="text" class="form-control input-large" id="loginSitename" placeholder="Site Name">
                        </div>

                        <div class="form-group login-username">
                            <input type="text" class="form-control input-large" id="loginUsername" placeholder="Username">
                        </div>

                        <div class="form-group login-password">
                            <input type="password" class="form-control input-large" id="loginPassword" placeholder="Password">
                        </div>

                        <div class="form-group submit">
                            <?php wp_nonce_field( 'ajax-login-nonce', 'wp_nonce' ); ?>
                            <button type="submit" class="login-button btn btn-primary btn-lg btn-thin-arrow-right" id="loginButton"><?php _e('Login', 'apiq'); ?></button>
                        </div>

                        <div class="form-bottom">
                            <div class="form-bottom-left">
                                <a href="<?php echo site_url('/forgotten-password?type=apiq'); ?>" class="btn btn-clear button-apiq"><?php _e('Forgotten Password?', 'apiq'); ?></a>
                                <a href="<?php echo site_url('/forgotten-password?type=associate'); ?>" class="btn btn-clear button-associate"><?php _e('Forgotten Password?', 'apiq'); ?></a>
                            </div>
                            <div class="form-bottom-right">
                                <button class="btn btn-clear button-associate login-toggle" data-login-type="APiQ Cloud">APiQ User</button>
                                <button class="btn btn-clear button-apiq login-toggle" data-login-type="Associate">Leica Biosystems User</button>
                            </div>
                        </div>

                    </div>

                </form>

                <div class="resource-links">

                    <a href="<?php echo site_url('/installation'); ?>" class="resource installation">
                        <h4><?php _e('Installation', 'apiq'); ?></h4>
                        <div class="resource-icon"></div>
                        <button class="btn btn-primary"><?php _e('View', 'apiq'); ?></button>
                    </a>

                    <a href="<?php echo site_url('/training'); ?>" class="resource training">
                        <h4><?php _e('Training', 'apiq'); ?></h4>
                        <div class="resource-icon"></div>
                        <button class="btn btn-primary"><?php _e('View', 'apiq'); ?></button>
                    </a>

                    <a href="<?php echo site_url('/apiq-support'); ?>" class="resource support">
                        <h4><?php _e('Support', 'apiq'); ?></h4>
                        <div class="resource-icon"></div>
                        <button class="btn btn-primary"><?php _e('View', 'apiq'); ?></button>
                    </a>

                    <a href="<?php echo site_url('/product-information'); ?>" class="resource product-information">
                        <h4><?php _e('Product Information', 'apiq'); ?></h4>
                        <div class="resource-icon"></div>
                        <button class="btn btn-primary"><?php _e('View', 'apiq'); ?></button>
                    </a>

                </div>

            </div>

        </div>

    </div>

    <div class="notification-bar" id="loginNotification">
        <div class="status-icon status-icon-medium status-icon-warning"></div>
        <h4>Login Error</h4>
        <div class="message">
            <p id="loginNotificationMessage"></p>
        </div>
    </div>

<?php

    endwhile; // end loop

    get_footer();
