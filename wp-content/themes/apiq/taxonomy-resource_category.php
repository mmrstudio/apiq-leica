<?php

    //print_r($wp_query); exit;

    $resource_categories = get_terms( array(
        'taxonomy' => 'resource_category',
        'hide_empty' => false,
    ));

    //print_r($resource_categories); exit;

    // check if user is logged in
    if( ! is_user_logged_in() ) :
        wp_redirect('/'); exit;
    endif;

    //print_r($wp_query); exit;
    $category_name = $wp_query->queried_object->name;

    // get translation
    $category_name = __($category_name, 'apiq');

    $cnt = 1;

    // prepare resources
    $resources = array();
    if ( have_posts() ) : while ( have_posts() ) : the_post();

        $resource_type = '';
        $resource_url = '';
        $video_files = array();

        // USER ACCESS
        $show_resource = true;
        $user_access = get_field('user_access');

        // determine is user can view
        if(in_array('view_lbs', $user_access) && !current_user_can('view_lbs')) $show_resource = false;
        if(in_array('view_lab', $user_access) && !current_user_can('view_lab')) $show_resource = false;

        // only include resource if user has access
        if($show_resource) :

            switch(get_field('resource_type')) :

                case 'Video' :

                    $resource_type = 'video';
                    $resource_url = '#';

                    // VIDEO FILES
                    $video = get_field('video');

                    foreach($video as $file) :

                        if(isset($file['video_file']['url']) && isset($file['video_file']['url'])) :

                            $video_files[] = array(
                                'url' => $file['video_file']['url'],
                                'mime' => $file['video_file']['mime_type']
                            );

                        endif;

                    endforeach;

                break;

                case 'PDF Download' :

                    $resource_type = 'pdf-download';

                    // PDF URL
                    $pdf_file = get_field('pdf_download');
                    $resource_url = $pdf_file['url'];

                break;

            endswitch;

            // get translations
            $locale = get_locale();
            if($locale != 'en_US') :
                $resource_title = (get_field('title_' . $locale)) ? get_field('title_' . $locale) : get_the_title();
                $resource_description = (get_field('description_' . $locale)) ? get_field('description_' . $locale) : apply_filters('the_content', get_the_content());
            else :
                $resource_title = get_the_title();
                $resource_description = apply_filters('the_content', get_the_content());
            endif;

            $resources[] = array(
                'id' => get_the_id(),
                'title' => $resource_title,
                'image' => get_the_post_thumbnail(get_the_id(), 'resource-thumb'),
                'description' => $resource_description,
                'resource_type' => $resource_type,
                'resource_url' => $resource_url,
                'video_files' => $video_files,
            );

        endif;

    endwhile; endif;

    $num_resources = count($resources);

    //print_r($resources); exit;

    get_header();

?>

    <div class="container resources-header">

        <h1><?php echo $category_name; echo ($num_resources > 0) ? ' (' . $num_resources . ')' : ''; ?></h1>

    </div>

    <div class="container resources">

        <div class="row">

        <?php

            if($num_resources > 0) :

                foreach($resources as $resource) :

                    $resource_classes = array();
                    if($cnt % 2 == 0) $resource_classes[] = 'col-2-last';
                    if($cnt % 4 == 0) $resource_classes[] = 'col-4-last';

            ?>

                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 resource <?php echo implode(' ', $resource_classes); ?> <?php echo $resource['resource_type']; ?>">

                    <a href="<?php echo $resource['resource_url']; ?>"<?php if($resource['resource_type'] == 'pdf-download') : ?> target="_blank"<?php endif; ?><?php if($resource['resource_type'] == 'video') : ?> data-video-files='<?php echo json_encode($resource['video_files']); ?>'<?php endif; ?>>

                        <div class="resource-thumbnail">
                            <?php echo $resource['image']; ?>
                        </div>

                        <h3><?php echo $resource['title']; ?></h3>

                        <div class="resource-description">
                            <?php echo $resource['description']; ?>
                        </div>

                    </a>

                </div>

            <?php

                    echo '<hr class="row-break col1 visible-xs hidden-sm hidden-md hidden-lg">'."\n";
                    echo ($cnt % 2 == 0) ? '<hr class="row-break col2 visible-sm hidden-xs hidden-md hidden-lg">'."\n" : '';
                    echo ($cnt % 3 == 0) ? '<hr class="row-break col3 visible-m hidden-xs hidden-sm hidden-lg">'."\n" : '';
                    echo ($cnt % 4 == 0) ? '<hr class="row-break col4 visible-lg hidden-xs hidden-sm hidden-md">'."\n" : '';

                    $cnt++;

                endforeach;

            else :

        ?>

        <div class="col-xs-12">
            <p><?php printf(__('There are no resources to show in %s', 'apiq'), '<strong>' . __($category_name, 'apiq') . '</strong>'); ?>.</p>
        </div>

        <?php endif; // end loop ?>

        </div>

    </div>

    <div class="modal" id="videoModal" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <video id="resourceVideo" class="video-js vjs-default-skin" controls preload="auto" width="720" height="405" poster=""></video>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><?php _e('Close', 'apiq'); ?></button>
                </div>
            </div>
        </div>
    </div>

<?php

    get_footer();
