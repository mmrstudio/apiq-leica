<?php

    // define cpt
    $cpt = array(
        'post_type_name' => 'resource',
        'singular' => 'Resource',
        'plural' => 'Resources',
        'slug' => 'resources'
    );

    // define cpt options
    $options = array(
        'supports' => array('title', 'editor', 'page-attributes', 'thumbnail'),
        'rewrite' => array(
            'slug' => 'resource',
            'pages' => false
        ),
        'has_archive' => true
    );

    // create cpt
    $resources_cpt = new CPT($cpt, $options);

    // set dashicon
    $resources_cpt->menu_icon('dashicons-format-video');

    // register category taxonomy
    $resources_cpt->register_taxonomy(array(
        'taxonomy_name' => 'resource_category',
        'singular' => 'Category',
        'plural' => 'Categories',
        'slug' => 'resources'
    ));

    // function to retrieve resource
    function get_resources($ppp=1, $order_by='post_date', $order='DESC') {

        $resources = array();

        // define query params
        $resources_query = array(
            'post_status' => 'publish',
            'post_type' => 'resource',
            'orderby' => $order_by,
            'order' => $order,
            'posts_per_page' => $ppp
        );

        // query db
        $get_resources = new WP_Query($resources_query);

        // get result
        $s = 1;
        if ( $get_resources->have_posts() ) : while ( $get_resources->have_posts() ) : $get_resources->the_post();

    		$resource = array(
    		    'id' => get_the_ID(),
    		    'permalink' => get_the_permalink(),
    		    'title' => get_the_title()
    		);

            $resources[] = $resource;

            $s++;

        endwhile; endif;
        
        wp_reset_query();

        return ($ppp > 1) ? $resources : $resources[0];

    }

    function get_resources_po() {

        $resources = array();

        // define query params
        $resources_query = array(
            'post_status' => 'publish',
            'post_type' => 'resource',
            'posts_per_page' => -1
        );

        // query db
        $get_resources = new WP_Query($resources_query);

echo '
msgid ""
msgstr ""
"Project-Id-Version: APiQ\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2015-04-14 11:08+1000\n"
"PO-Revision-Date: 2015-04-14 11:20+1000\n"
"Last-Translator: \n"
"Language-Team: MMR Studio <web@mmr.com.au>\n"
"Language: en_US\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.7.5\n"
"X-Poedit-SourceCharset: UTF-8\n"

';

        // get result
        if ( $get_resources->have_posts() ) : while ( $get_resources->have_posts() ) : $get_resources->the_post();

            echo '#: resource_title:' . get_the_id() . "\n";
            echo 'msgid "'. addslashes(get_the_title()) .'"' . "\n";
            echo 'msgstr ""' . "\n";
            echo "\n";
            echo '#: resource_description:' . get_the_id() . "\n";
            echo 'msgid "'. addslashes(get_the_content()) .'"' . "\n";
            echo 'msgstr ""' . "\n";
            echo "\n";

        endwhile; endif;
        
        wp_reset_query();

        exit;

    }

    //get_resources_po();

    function parse_resources_po() {

        require_once (TEMPLATEPATH . '/includes/classes/PHP-po-parser-master/src/Sepia/InterfaceHandler.php');
        require_once (TEMPLATEPATH . '/includes/classes/PHP-po-parser-master/src/Sepia/FileHandler.php');
        require_once (TEMPLATEPATH . '/includes/classes/PHP-po-parser-master/src/Sepia/PoParser.php');
        require_once (TEMPLATEPATH . '/includes/classes/PHP-po-parser-master/src/Sepia/StringHandler.php');

        $file = $_SERVER['DOCUMENT_ROOT'] . '/content_de_DE.po';

        //echo $file; exit;

        $fileHandler = new Sepia\FileHandler($file);
        
        $poParser = new Sepia\PoParser($fileHandler);
        $entries  = $poParser->parse();

        print_r($entries);

        exit;

    }

    //parse_resources_po();

