<?php
    
if( function_exists('register_field_group') ):

$languages = array(
    'de_DE' => 'German',
    'fr_FR' => 'French',
    'it_IT' => 'Italian',
    'es_ES' => 'Spanish',
);


$field_group = array (
	'key' => 'group_552b65dbbd521',
	'title' => 'Translations',
	'fields' => array (),
	'location' => array (
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'resource',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
);

foreach($languages as $locale => $lang) :

		$field_group['fields'][] = array (
			'key' => 'field_' . md5($lang . ' (' . $locale . ')'),
			'label' => $lang . ' (' . $locale . ')',
			'name' => '',
			'prefix' => '',
			'type' => 'tab',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
		);

		$field_group['fields'][] = array (
			'key' => 'field_' . md5('title_' . $locale),
			'label' => 'Title',
			'name' => 'title_' . $locale,
			'prefix' => '',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		);

		$field_group['fields'][] = array (
			'key' => 'field_' . md5('description_' . $locale),
			'label' => 'Description',
			'name' => 'description_' . $locale,
			'prefix' => '',
			'type' => 'wysiwyg',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'tabs' => 'all',
			'toolbar' => 'full',
			'media_upload' => 0,
		);

		$field_group['fields'][] = array (
			'key' => 'field_' . md5('alternative_content_' . $locale),
			'label' => 'Alternative Content?',
			'name' => 'alternative_content_' . $locale,
			'prefix' => '',
			'type' => 'true_false',
			'instructions' => 'Do you need to provide alternative versions of the resource for this language? This will override the options set above when this language is selected by the user.',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'message' => '',
			'default_value' => 0,
		);

		$field_group['fields'][] = array (
			'key' => 'field_' . md5('resource_type_' . $locale),
			'label' => 'Resource Type',
			'name' => 'resource_type_' . $locale,
			'prefix' => '',
			'type' => 'radio',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_552b66c9a27d8',
						'operator' => '==',
						'value' => '1',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				'Video' => 'Video',
				'PDF Download' => 'PDF Download',
			),
			'other_choice' => 0,
			'save_other_choice' => 0,
			'default_value' => '',
			'layout' => 'vertical',
		);

		$field_group['fields'][] = array (
			'key' => 'field_' . md5('pdf_download_' . $locale),
			'label' => 'PDF Download',
			'name' => 'pdf_download_' . $locale,
			'prefix' => '',
			'type' => 'file',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_552b66c9a27d8',
						'operator' => '==',
						'value' => '1',
					),
					array (
						'field' => 'field_552b6714a27db',
						'operator' => '==',
						'value' => 'PDF Download',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'return_format' => 'array',
			'library' => 'all',
		);

		$field_group['fields'][] = array (
			'key' => 'field_' . md5('video_' . $locale),
			'label' => 'Video',
			'name' => 'video_' . $locale,
			'prefix' => '',
			'type' => 'repeater',
			'instructions' => 'Videos require at least two formats to be uploaded; mp4 and ogg/ogv, with an optional third format; webm.',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_552b66c9a27d8',
						'operator' => '==',
						'value' => '1',
					),
					array (
						'field' => 'field_552b6714a27db',
						'operator' => '==',
						'value' => 'Video',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'min' => '',
			'max' => '',
			'layout' => 'table',
			'button_label' => 'Add Video File',
			'sub_fields' => array (
				array (
					'key' => 'field_552b67ef3a8ba',
					'label' => 'Video File',
					'name' => 'video_file_' . $locale,
					'prefix' => '',
					'type' => 'file',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'return_format' => 'array',
					'library' => 'all',
				),
			),
		);

endforeach;

register_field_group($field_group);

endif;