<?php

    get_header();

    if ( have_posts() ) while ( have_posts() ) : the_post();

?>

    <div class="container resources-header">

        <h1 class="content-heading"><?php the_title(); ?></h1>

    </div>

    <div class="container content">

        <div class="row">

            <div class="col-xs-12">
                <?php the_content(); ?>
            </div>

        </div>

    </div>


<?php

    endwhile;

    get_footer();
