<?php

    /* Template Name: Page - Reset Password */

    get_header();

    if ( have_posts() ) while ( have_posts() ) : the_post();

?>

    <div class="container no-padding">

        <div class="home-hero">
            <div class="apiq-title"></div>
            <div class="apiq-banner"></div>
            <div class="leica-brand-stripe"></div>
        </div>

    </div>

    <div class="container home-main">

        <div class="row title-row">

            <div class="col-xs-6 title-col home-left-col">
                <h1><?php _e('Update Password', 'apiq'); ?></h1>
            </div>

            <div class="col-xs-6 title-col home-right-col">
            </div>

        </div>

        <div class="row content-row">

            <div class="col-xs-6 intro home-left-col" id="introCol">
                <?php echo sprintf(
                    '<p>%s</p><p><strong>%s</strong><br>%s<br>%s<br>%s<br>%s<br>%s<br>%s</p>',
                    __('Your APiQ.com password has expired, please set a new password.', 'apiq'),
                    __('Your password must have:', 'apiq'),
                    __('Minimum of 8 characters', 'apiq'),
                    __('At least 1 upper case letter', 'apiq'),
                    __('At least 1 lower case letter', 'apiq'),
                    __('At least 2 numbers', 'apiq'),
                    __('At least 2 special characters', 'apiq'),
                    __('Never been used before', 'apiq')
                ); ?>
            </div>

            <div class="col-xs-6 home-right-col login-resource-col">

                <form action="/" class="change-password-form" id="changeAPiQPasswordForm">

                    <div class="form-group login-site-name">
                        <input type="text" class="form-control input-large" id="changeAPiQpasswordSite" placeholder="<?php _e('Site Name', 'apiq'); ?>" value="<?php echo (isset($_GET['site'])) ? $_GET['site'] : ''; ?>">
                    </div>

                    <div class="form-group login-username">
                        <input type="text" class="form-control input-large" id="changeAPiQpasswordUsername" placeholder="<?php _e('Username', 'apiq'); ?>" value="<?php echo (isset($_GET['username'])) ? $_GET['username'] : ''; ?>">
                    </div>

                    <div class="form-group login-password">
                        <input type="password" class="form-control input-large" id="changeAPiQpasswordOld" placeholder="<?php _e('Current Password', 'apiq'); ?>">
                    </div>

                    <div class="form-group login-password">
                        <input type="password" class="form-control input-large" id="changeAPiQpasswordNew" placeholder="<?php _e('New Password', 'apiq'); ?>">
                    </div>

                    <div class="form-group submit">
                        <button type="submit" class="login-button btn btn-primary btn-lg btn-thin-arrow-right" id="changeAPiQPasswordButton"><?php _e('Submit', 'apiq'); ?></button>
                    </div>

                </form>

            </div>

        </div>

    </div>

<?php

    endwhile; // end loop

    get_footer();
